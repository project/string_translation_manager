<?php

namespace Drupal\string_translation_manager\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Component\Serialization\Json;

/**
 * Configure locale settings for this site.
 *
 * @internal
 */
class StringTranslationManagerAddForm extends ConfigFormBase {

  /**
   * Context Name Default.
   */
  const CONTEXT_NAME_DEFAULT = 'stringTranslationManager';

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'string_translation_manager_add_form';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['string_translation_manager.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    // We'll use this config on description @codingStandardsIgnoreLine.
    $config = $this->config('string_translation_manager.settings');

    $form['strings'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Strings to be added'),
      '#required' => TRUE,
    ];

    $contextDescription = $this->t('If you leave this field blank the context will be: <b>@contextNameDefault</b><br>', [
      '@contextNameDefault' => self::CONTEXT_NAME_DEFAULT,
    ]);

    if (!empty($config->get('contexts_used'))) {

      $contextDescription .= $this->t('Contexts that have been previously used:');

      $contextsUsed = Json::decode($config->get('contexts_used'));

      $contextDescription .= '<ul>';

      foreach ($contextsUsed as $context) {
        $contextDescription .= '<li>' . $context . '</li>';
      }

      $contextDescription .= '</ul>';
    }

    $form['context'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Context'),
      '#description' => $contextDescription,
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {

    parent::validateForm($form, $form_state);

    if (empty($form_state->getValue('strings'))) {
      $form_state->setErrorByName('strings', $this->t('You must enter strings.'));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

    $values = $form_state->getValues();

    $stringsToTranslate = explode(PHP_EOL, $values['strings']);

    $context = self::CONTEXT_NAME_DEFAULT;
    if (!empty($values['context'])) {
      $context = trim($values['context']);
    }

    foreach ($stringsToTranslate as $stringToTranslate) {

      $stringToTranslate = trim($stringToTranslate);

      // We need translate this textarea @codingStandardsIgnoreLine.
      $this->t($stringToTranslate, [], ['context' => $context]);
    }

    $config = $this->config('string_translation_manager.settings');

    $contextsUsed = [];

    if (!empty($config->get('contexts_used'))) {
      $contextsUsed = Json::decode($config->get('contexts_used'));
    }

    if (empty(array_search($context, $contextsUsed))) {
      array_push($contextsUsed, $context);
    }

    $contextsUsed = Json::encode($contextsUsed);

    $config->set('contexts_used', $contextsUsed);

    $config->save();

    $this->messenger()->addMessage($this->t('Strings have been added'));
  }

}
